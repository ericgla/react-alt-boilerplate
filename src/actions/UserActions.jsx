import alt from '../alt';

class UserActions {
    login(credentials) {
        this.dispatch(credentials);
    }
}

module.exports = alt.createActions(UserActions);