import React from 'react';
import { Route, DefaultRoute } from 'react-router';

// You must import all of the components that represent route handlers
import Home from './routes/Home';
import App from './routes/App';
import AuthApp from './components/AuthApp';
import Login from './routes/Login';
import Dashboard from './routes/Dashboard';
import Flavors from './routes/Flavors';

function requireAuth(nextState, transition) {
  if (!auth.loggedIn())
    transition.to('/login', null, { nextPathname: nextState.location.pathname });
}

export default (
  <Route path="/" handler={App}>
    <DefaultRoute name="home" handler={Flavors} />
    <Route name="flavors" path="flavors" handler={Flavors} />
  </Route>
);