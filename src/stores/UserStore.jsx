import alt from '../alt';
import UserActions from '../actions/UserActions';

class UserStore {

    constructor() {
        this.bindListeners({
            handleLogin: UserActions.LOGIN
        });
    }

    handleLogin(credentials) {
        this.setState({
            authenticated: true
        });
    }
}

module.exports = alt.createStore(UserStore, 'UserStore');