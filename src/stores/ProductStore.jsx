import alt from '../alt';
import ProductActions from '../actions/ProductActions';

class ProductStore {

    constructor() {
        this.bindListeners({
            handleFlavors: ProductActions.FLAVORS
        });

    }

    handleFlavors() {
        var _this = this;

        return fetch("https://api.whaxy.com/flavors", {
            method: "GET",
            //headers: headers,
            //body: JSON.stringify(params.body)
        })
        .then(function(response) {
            console.log(`response code ${response.status}`);
            return response.json();
        })
        .then(function(json) {
            console.log("setting flavors in store");
            //this triggers a change event, which will fire the handler in the component
            _this.setState({
                flavors: json
            });
        });
    }
}

module.exports = alt.createStore(ProductStore, 'ProductStore');