import React from 'react';
import { RouteHandler, Link } from 'react-router';

import ProductActions from '../actions/ProductActions';
import ProductStore from '../stores/ProductStore';

export default class Flavors extends React.Component {

    componentDidMount() {
        ProductStore.listen(this._onFlavors.bind(this));
        ProductActions.flavors();
    }

    componentWillUnmount() {
        ProductStore.unlisten(this._onFlavors);
    }

    _onFlavors(state) {
        this.setState ({
            flavors: state.flavors
        });
    }

    render() {
        console.log("rendering flavors");
        if (!this.state || !this.state.flavors)
            return null;

        var flavors = this.state.flavors.map(function(flavor) {
            return <p>{flavor.slug}</p>;
        })

        return (
            <div className='container-fluid'>
                {flavors}
            </div>
        );
    }
}

Flavors.contextTypes = {
    router: React.PropTypes.func.required
};