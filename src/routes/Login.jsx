import * as less from './Login.less';
import React from 'react';
import { RouteHandler, Link } from 'react-router';

import UserActions from '../actions/UserActions';
import UserStore from '../stores/UserStore';

// A route handler that contains the entirety of the application.
export default class Login extends React.Component {

    constructor() {
        super();
        this.state = UserStore.getState();
    }

    componentDidMount() {
        UserStore.listen(this._onLogin.bind(this));
    }

    componentWillUnmount() {
        UserStore.unlisten(this._onLogin);
    }

    _login() {
        var email = React.findDOMNode(this.refs.email).value;
        var password = React.findDOMNode(this.refs.password).value;

        UserActions.login({
            email,
            password
        });
    }

    _onLogin(state) {
        if (state.authenticated) {
            console.log('authenticated!');
            this.context.router.transitionTo("dashboard");
        }
    }

    render() {
        return (
            <div className="container">
                <form className="form-signin">
                    <h1 className="form-signin-heading text-muted">Sign In</h1>
                    <input type="text" ref="email" className="form-control" placeholder="Email address" required="" autofocus="" />
                    <input type="password" ref="password" className="form-control" placeholder="Password" required="" />
                    <button className="btn btn-lg btn-primary btn-block" type="submit" onClick={this._login.bind(this)}>
                        Sign In
                    </button>
                </form>
            </div>
        );
    }
}

Login.contextTypes = {
    router: React.PropTypes.func.required
};