import React from 'react';
import { RouteHandler, Link } from 'react-router';

// A route handler that represents the Home route.
export default class Home extends React.Component {
  render() {
    return (
      <div className='container-fluid'>
        <div className='page-header'>
          <h1>Unauthenticated</h1>
          <Link to='login' className='btn btn-warning'>Log In</Link>
        </div>
      </div>
    );
  }
}

Home.contextTypes = {
  router: React.PropTypes.func.required
};