import React from 'react';
import UserStore from '../stores/UserStore';
import { RouteHandler, Link } from 'react-router';

export default class AuthApp extends React.Component {

    constructor() {
        super();
        this.state = UserStore.getState();
    }

    componentWillMount() {
        if (this.state.authenticated || this.state.authenticated === false) {
            this.context.router.transitionTo("login");
        }
    }

    render() {
        return <RouteHandler />
    }
}

AuthApp.contextTypes = {
    router: React.PropTypes.func.required
};